package com.atguigu.service;

import com.atguigu.pojo.TravelItem;

/**
 * @author captainlhy
 * @create 2021-05-31 22:33
 */
public interface TravelItemService {
    void add(TravelItem travelItem);
}
