package com.atguigu.dao;

import com.atguigu.pojo.TravelItem;

/**
 * @author captainlhy
 * @create 2021-05-31 22:35
 */
public interface TravelItemDao {
    void add(TravelItem travelItem);
}
